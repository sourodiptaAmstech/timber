<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('stock', function(Blueprint $table) {
			$table->foreign('timber_id')->references('timber_id')->on('timber')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('stock', function(Blueprint $table) {
			$table->foreign('location_id')->references('location_id')->on('location')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('print_logs', function(Blueprint $table) {
			$table->foreign('timber_id')->references('timber_id')->on('timber')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('print_logs', function(Blueprint $table) {
			$table->foreign('stock_id')->references('stock_id')->on('stock')
						->onDelete('restrict')
						->onUpdate('restrict');
        });
        Schema::table('users', function(Blueprint $table) {
			$table->foreign('position')->references('id')->on('position')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('movement', function(Blueprint $table) {
			$table->foreign('stock_id')->references('stock_id')->on('stock')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('movement', function(Blueprint $table) {
			$table->foreign('location_id')->references('location_id')->on('location')
						->onDelete('restrict')
						->onUpdate('restrict');
        });
        Schema::table('movement', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
        });
	}

	public function down()
	{
		Schema::table('stock', function(Blueprint $table) {
			$table->dropForeign('stock_timber_id_foreign');
		});
		Schema::table('stock', function(Blueprint $table) {
			$table->dropForeign('stock_location_id_foreign');
		});
		Schema::table('print_logs', function(Blueprint $table) {
			$table->dropForeign('print_logs_timber_id_foreign');
		});
		Schema::table('print_logs', function(Blueprint $table) {
			$table->dropForeign('print_logs_stock_id_foreign');
        });

	}
}
