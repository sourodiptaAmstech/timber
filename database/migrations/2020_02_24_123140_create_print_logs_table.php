<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePrintLogsTable extends Migration {

	public function up()
	{
		Schema::create('print_logs', function(Blueprint $table) {
			$table->bigIncrements('print_logs_id', true);
			$table->bigInteger('timber_id')->unsigned();
			$table->bigInteger('user_id')->unsigned();
			$table->bigInteger('stock_id')->unsigned();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::dropIfExists('print_logs');
	}
}
