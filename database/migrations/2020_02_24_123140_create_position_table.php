<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePositionTable extends Migration {

	public function up()
	{
		Schema::create('position', function(Blueprint $table) {
			$table->bigIncrements('id', true);
			$table->string('name', 500);
			$table->tinyInteger('active')->default('1');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::dropIfExists('position');
	}
}
