<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStockTable extends Migration {

	public function up()
	{
		Schema::create('stock', function(Blueprint $table) {
			$table->bigIncrements('stock_id', true);
			$table->bigInteger('timber_id')->unsigned();
			$table->bigInteger('location_id')->unsigned();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::dropIfExists('stock');
	}
}
