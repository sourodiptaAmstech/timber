<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTimberTable extends Migration {

	public function up()
	{
		Schema::create('timber', function(Blueprint $table) {
			$table->bigIncrements('timber_id', true);
			$table->string('name', 500);
			$table->bigInteger('number')->default('0');
			$table->string('bar_code', 255)->unique();
			$table->tinyInteger('active')->default('1');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::dropIfExists('timber');
	}
}
