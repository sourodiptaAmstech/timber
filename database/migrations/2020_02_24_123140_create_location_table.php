<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLocationTable extends Migration {

	public function up()
	{
		Schema::create('location', function(Blueprint $table) {
			$table->bigIncrements('location_id', true);
			$table->string('name', 255);
			$table->string('number', 255);
            $table->tinyInteger('active')->default('1');
            $table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::dropIfExists('location');
	}
}
