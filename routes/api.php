<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
  //  return $request->user();
//});
Route::post('/register','Api\AuthController@register');
Route::post('/login','Api\AuthController@login');
Route::put('/forgetpass','Api\ForgetPasswordController@forgetPassword');
Route::get('download-pdf/{id}/{barcode}', 'Api\PrintLogsController@generatePDF')->name('download-pdf');

Route::middleware('auth:api')->group( function () {
   Route::resource('location', 'Api\LocationController');
   Route::resource('position', 'Api\PositionController');
   Route::resource('stock', 'Api\StockController');
   Route::post('stock/delete', 'Api\StockController@massDestroy');
   Route::resource('print/logs', 'Api\PrintLogsController');
   
   Route::resource('timber', 'Api\TimberController');
   Route::post('timber/movement', 'Api\TimberController@timberMovement');
   Route::post('timber/massmovement', 'Api\TimberController@timberMassMovement');

   Route::post('search/timber', 'Api\TimberController@search')->name('api.timber.search');
   Route::get('timber/location/{location_id}', 'Api\TimberController@timberByLocation')->name('api.timber.location');
   
   Route::post('/upload','Api\AuthController@uploadProfileImage');
   Route::get('/location-map/images','Api\LocationMapController@getLocationMapImage');
   
   Route::post('timber/barcode', 'Api\TimberController@searchByBarcode')->name('api.timber');

   Route::post('/device/token','Api\AuthController@storeDeviceToken')->name('device.token');
   
   
});
