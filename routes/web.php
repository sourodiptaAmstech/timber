<?php

/*
|--------------------------------------------------------------------------
//AoUl$I1
| Web Routes 
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Admin\LoginController@index')->name('/');
Route::post('login', 'Admin\LoginController@login')->name('login');
Route::get('forget-password', 'Admin\PasswordController@forgetPasswordIndex')->name('forget-password');
Route::post('forget-password', 'Admin\PasswordController@forgetPassword')->name('forget-password');

#auth
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
	Route::post('logout', 'LoginController@logout')->name('logout');

	Route::get('home', 'HomeController@index')->name('home');

	Route::resource('users', 'UserController');
	Route::post('users/status/{id}', 'UserController@changeStatus')->name('users.status');

	Route::resource('positions', 'PositionController');
	
	Route::post('positions/active/{id}', 'PositionController@changeStatus')->name('positions.active');

	Route::post('change-password', 'PasswordController@changePassword')->name('change-password');

	Route::resource('timbers', 'TimberWebController');
	Route::post('timber/search', 'TimberWebController@searchTimber')->name('timber.search');

	Route::resource('locations', 'AdminLocationController');
	Route::get('location/list', 'AdminLocationController@getLocation')->name('locations.list');

	Route::resource('location-map', 'LocationMapController');
	Route::resource('printlogs', 'PrintlogsController');

	Route::post('import-csv','TimberWebController@importCSVFile')->name('import-csv');
	
});