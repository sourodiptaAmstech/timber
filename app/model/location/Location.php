<?php

namespace App\model\location;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\model\timber\Timber;


class Location extends Model
{

    protected $table = 'location';
    protected $primaryKey = 'location_id';
    public $timestamps = true;
    protected $fillable = [
        'name', 'number',
    ];


    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function stocks()
    {
        return $this->hasMany('App\model\stock\Stock', 'location_id', 'location_id');
    }

}
