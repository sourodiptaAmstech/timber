<?php

namespace App\model\movement;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Movement extends Model 
{

    protected $table = 'movement';
    protected $primaryKey = 'movement_id';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

}