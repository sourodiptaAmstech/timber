<?php

namespace App\model\stock;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stock extends Model 
{

    protected $table = 'stock';
    protected $primaryKey = 'stock_id';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function timbers()
    {
        return $this->belongsTo('App\model\timber\Timber');
    }

    public function locations()
    {
        return $this->belongsTo('App\model\location\Location');
    }

}