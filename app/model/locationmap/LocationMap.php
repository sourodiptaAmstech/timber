<?php

namespace App\model\locationmap;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class LocationMap extends Model
{
    protected $table = 'location_map';
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    
}
