<?php

namespace App\model\position;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Position extends Model 
{

    protected $table = 'position';

    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'active'
    ];

    public function users()
    {
        return $this->hasMany('App\User', 'position', 'id');
    }

}