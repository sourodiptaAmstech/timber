<?php

namespace App\model\timber;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\model\location\Location;


class Timber extends Model 
{

    protected $table = 'timber';
    protected $primaryKey = 'timber_id';
    public $timestamps = true;

    protected $fillable = [
        'number',
        'bar_code',
    ];

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function stocks()
    {
        return $this->hasOne('App\model\stock\Stock', 'timber_id', 'timber_id');
    }

}