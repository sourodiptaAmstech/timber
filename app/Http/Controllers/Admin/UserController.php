<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\AuthController;
use Exception;
use App\model\position\Position;
use App\User;
use App\Notifications\PushNotification;
use URL;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('account_type', 1)->orderBy('id', 'desc')->get();
        return view('user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $positions = Position::where('active',1)->orderBy('name', 'asc')->get();
        return view('user.create', compact('positions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request["account_type"]=1;
            $auth_obj = new AuthController;
            $register = $auth_obj->register($request);
            
            return response(['success'=>true, 'message'=>"User successfully registered ","errors"=>(object)array()],201);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::where('id', $id)->delete();
        return response(['success'=>true, 'message'=>"User deleted successfully ","errors"=>(object)array()],201);  
    }

    public function changeStatus(Request $request, $id)
    {
        $user = User::where('id', $id)->where('account_type', 1)->first();

        if ($user->status == 1) {
            $user->status = 0;

            $FCMTokens = [];
            $PushNotification = "";
            $pushnotificationsData = [];

            $FCMTokens['Android'][]=$user->device_token;
            $pushnotificationsData['to'] = $FCMTokens;
            $pushnotificationsData['notification'] = array(
                'title' => 'USER DEACTIVATION ALERT',
                'text' => '',
                'click_action' => '',
                'body'=>'User has been deactivated.',
                'picture_url'=>URL::asset('images/block-user.png')
            );
            $pushnotificationsData['data'] = [
                "type"=>"USERDEACTIVATIONALERT",
                'title' => 'USER DEACTIVATION ALERT',
                'text' => '',
                'click_action' => '',
                'body'=>'User has been deactivated.',
                'picture_url'=>URL::asset('images/block-user.png')
            ];
            
            $PushNotification=PushNotification::PushNotifications($pushnotificationsData);

        } else {
            $user->status = 1;
        }
        $user->save();

        return response()->json([
            'success' => true,
            'message' => 'Status updated successfully'
        ], 200);
    }

}
