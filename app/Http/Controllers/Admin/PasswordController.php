<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\ForgetPasswordController;
use Exception;
use Illuminate\Support\Facades\Hash;
use Auth;


class PasswordController extends Controller
{
    public function forgetPasswordIndex()
    {
        return view('auth.forgetpassword');
    }

    public function forgetPassword(Request $request){
        try {
            $forgetPassWord=new ForgetPasswordController;
            $result = $forgetPassWord->forgetPassword($request);
          
            if($result->original['success']){
                return response(['success' => true, 'message'=>$result->original['message'],"errors"=>(object)array()],201);
            }
            return response(['message'=>$result->original['message'],"errors"=>array("email"=>["Couldn't find your Timber Account"])],404);
            
        }
        catch(\Illuminate\Database\QueryException  $e) {
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }

    }

    public function changePassword(Request $request)
    {
        $validatedData = $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|string|min:6|same:confirm_new_password',
            'confirm_new_password' => 'required'
        ]);

        if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
            return response()->json([
                'message' => 'Your current password does not matches with the password you provided. Please try again.'
            ],404);
            
        }
        if(strcmp($request->get('current_password'), $request->get('new_password')) == 0){
            return response()->json([
                'message' => 'New Password cannot be same as your current password. Please choose a different password.'
            ],404);
           
        }
     
        $user = Auth::user();
        $user->password = bcrypt($request->get('new_password'));
        $user->save();

        return response()->json([
            'success' => true,
            'message' => 'Password changed successfully!'
        ],201);

    }
}

