<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\position\Position;
use App\User;
use Validator;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;



class PositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $positions = Position::orderBy('id', 'desc')->get();
        return view('position.index', compact('positions'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('position.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->getMessageBag(),
            ], 422);
        }

        $obj = new Position;
        $obj->name = $request->name;
        $obj->save();
        
        return response()->json([
            'success' => true,
            'message' => 'Position Created Successfully.',
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $position = Position::find($id);
        return view('position.edit', compact('position'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ids)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'active' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->getMessageBag(),
            ], 422);
        }
        $obj = Position::find($request->id);
        $obj->name = $request->name;
        $obj->active = $request->active;
        $obj->save();
       
        return response()->json([
            'success' => true,
            'message' => 'Position Updated Successfully.',
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::where('position', $id)->get();
        if ($user->count() > 0){

            return response()->json([
                'success' => false,
                'message' => 'Position is asigned to a user. Does not allow to delete position',
            ], 404);

        } else {
           
            Position::where('id', $id)->delete();
        }
    }

    public function changeStatus(Request $request, $id)
    {
        $position = Position::find($id);

        if ($position->active == 1) {
            $position->active = 0;
        } else {
            $position->active = 1;
        }
        $position->save();

        return response()->json([
            'success' => true,
            'message' => 'Status updated successfully'
        ], 200);
    }
}
