<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    protected $redirectTo = 'admin/home';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index()
    {
    	return view('auth.login');
    }

    public function login(Request $request)
    {
    	$request->validate([
                'email'=>'email|required',
                'password'=>'required'
            ]);
    
    	if(!Auth::attempt(['email' => $request->email, 'password' => $request->password, 'account_type' => 0]))
        {
            return redirect()->route('/')->with('message', 'Invalid credentials');

        } else {
        	return redirect()->route('admin.home');
        }

    }

    public function logout(Request $request) {

        Auth::logout();
        return redirect()->route('/');

    }

}
