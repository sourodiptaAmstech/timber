<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\LocationController;
use App\model\location\Location;
use Validator;


class AdminLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $location_obj = new LocationController;
        $locations = $location_obj->index($request);
        
        if ($locations->original['success'] == true) {
            return response()->json([
                'success' => true,
                'data' => $locations->original['data'],
            ],200);
        }
        if ($locations->original['success'] == false) {
            return response()->json([
                'success' => false,
                'message'=>"Location not available"
            ],404);
        }
    }

    public function getLocation(Request $request)
    {
        $location = Location::orderBy("number", "asc")->pluck("number","location_id");
        return response()->json($location);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $location = new LocationController;
        $location = $location->store($request);

        return response()->json([
            'success' => true,
            'message' => 'Location added successfully',
            'location' => $location
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $ids)
    {
        $location = Location::where('location_id', $request->id)->first();
        return response()->json([
            'success' => true,
            'location' => $location
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ids)
    {
        
        $validator = Validator::make($request->all(),[
            // 'name'      => 'required',
            'number'    => 'required|alpha_num|unique:location,number,'.$request->id.',location_id',
            //'active'    => 'required'
         ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->getMessageBag()
             ],422);
        }
        
        $location = Location::where('location_id', $request->id)->first();
        // $location->name       = $request->name;
        $location->number     = $request->number;
       // $location->active     = $request->active;
        $location->save();

        return response()->json([
            'success' => true,
            'message' => 'Location updated successfully'
         ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
