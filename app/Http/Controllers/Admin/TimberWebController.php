<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\timber\Timber;
use App\model\location\Location;
use App\model\stock\Stock;
use Validator;
use Illuminate\Validation\Rule;
use App\model\movement\Movement;
use Illuminate\Support\Facades\Auth;
use DB;

class TimberWebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $timbers = Timber::join('stock', 'timber.timber_id', '=', 'stock.timber_id')
                ->join('location', 'stock.location_id', '=', 'location.location_id')
                ->select('timber.*', 'location.name as location_name', 'location.number as location_number', 'stock.deleted_at as stock_deleted_at')
                ->orderBy('timber_id', 'desc')->get();
        // dd($timbers);
        return view('timber.index', compact('timbers'));
    }

    public function searchTimber(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'timber_number'      => 'required',
            'location_number'    => 'required',
         ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->getMessageBag()
             ],422);
        }

        $timber_number = $request->timber_number;
        $location_number = $request->location_number;

        $timbers = Timber::join('stock', 'timber.timber_id', '=', 'stock.timber_id')
                ->join('location', 'stock.location_id', '=', 'location.location_id')
                ->select('timber.*', 'location.name as location_name', 'location.number as location_number')
                ->where('stock.deleted_at', null)
                ->where('timber.number', 'LIKE', '%'.$timber_number.'%')
                ->where('location.number', 'LIKE', '%'.$location_number.'%')
                ->orderBy('timber_id', 'desc')->get();
       
        return response()->json([
            'success' => true,
            'timbers' => $timbers
        ],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $location = Location::first();
        return view('timber.create', compact('location'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::id();
        $validator = Validator::make($request->all(),[
            // 'name'      => 'required',
            'number'    => 'required|integer|digits:6|unique:timber,number',
            // 'bar_code'  => 'required|unique:timber',
            // 'location'  => 'required',
            // 'grade'     => 'required|alpha_num|size:5',

         ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->getMessageBag()
             ],422);
        }
        
        $timber_obj = new Timber;
        // $timber_obj->name       = $request->name;
        $timber_obj->number     = $request->number;
        $timber_obj->bar_code   = $request->number;
        // $timber_obj->grade      = $request->grade;
        $timber_obj->save();
       
        $stock_obj = new Stock;
        $stock_obj->timber_id   = $timber_obj->timber_id;
        $stock_obj->location_id = $request->location;
        $stock_obj->save();

        $movement = new Movement;
        $movement->stock_id = $stock_obj->stock_id;
        $movement->location_id = $stock_obj->location_id;
        $movement->user_id =  $user_id;
        $movement->save();

        return response()->json([
            'success' => true,
            'message' => 'Timber Added Successfully',
            'data' => $timber_obj
        ],200);

    }

    public function importCSVFile(Request $request)
    {
        $user_id = Auth::id();
        $request->validate([
            'file'=>'required|mimes:csv,txt'
        ]);
        $csv_file=$request->file;

        $fileD = fopen($csv_file,"r");
        
        while(!feof($fileD)){
            $rowData[]=fgetcsv($fileD);
        }
        foreach ($rowData as $key => $value) {
            if(isset($value[1])){
                return response([
                    'success'=>false,
                    'message'=>"Invalid file format!",
                ],401);
            }
            if (isset($value[0])) {
                $val = (integer)$value[0];
                $num = (integer)ceil(log10($val));
                
                if($num == 6){
                    $timber_data[]=array('number'=>$val,'bar_code'=>$val);

                } else {
                    return response([
                        'success'=>false,
                        'message'=>"Invalid data present in csv file!",
                    ],401);
                }
            } 
        }

        Timber::insert($timber_data);
        $last_timber_id = DB::getPDO()->lastInsertId();
        foreach($timber_data as $d){
            $stock_data[]=array('timber_id'=>$last_timber_id++,'location_id'=>$request->location);
        }

        Stock::insert($stock_data);
        $last_stock_id = DB::getPDO()->lastInsertId();
        foreach($timber_data as $d){

            $movement_data[]=array('stock_id'=>$last_stock_id++,'location_id'=>$request->location,'user_id'=>$user_id);
        }

        Movement::insert($movement_data);

        return response([
            'success'=>true,
            'message'=>"CSV file imported successfully",
            "data"=>(object)[],
            "errors"=>array("exception"=>["Everything is OK."])
        ],201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $locations = Location::orderBy('number', 'asc')->get();
        $timber = Timber::where('timber_id', $id)->first();
        return view('timber.edit', compact('locations', 'timber'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ids)
    {
        $user_id = Auth::id();
        $validator = Validator::make($request->all(),[
            // 'name'      => 'required',
            'number'    => 'required|integer|digits:6|unique:timber,number,'.$request->id.',timber_id',
            // 'bar_code'  => 'required|unique:timber,bar_code,'.$request->id.',timber_id',
            'location'  => 'required',
            'active'    => 'required',
            // 'grade'     => 'required|alpha_num|size:5',
         ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->getMessageBag()
             ],422);
        }
        
        $timber = Timber::where('timber_id', $request->id)->first();
        // $timber->name       = $request->name;
        $timber->number     = $request->number;
        $timber->bar_code   = $request->bar_code;
        $timber->active     = $request->active;
        // $timber->grade      = $request->grade;
        $timber->save();

        $isDeleteStock = Stock::where('timber_id', $request->id)->where('deleted_at', NULL)->first();
        if ($isDeleteStock) {
        $stock = Stock::where('timber_id', $request->id)->first();
        $stock->location_id = $request->location;
        $stock->save();
        } else {
            $stock = Stock::where('timber_id', $request->id)->onlyTrashed()->first();
            $stock->location_id = $request->location;
            $stock->deleted_at = Null;
            $stock->save();
        }

        $movement = new Movement;
        $movement->stock_id = $stock->stock_id;
        $movement->location_id = $stock->location_id;
        $movement->user_id =  $user_id;
        $movement->save();

        return response()->json([
            'success' => true,
            'message' => 'Timber updated successfully'
         ],200);

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
