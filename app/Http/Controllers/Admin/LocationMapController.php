<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\locationmap\LocationMap;

class LocationMapController extends Controller
{
    /**
     * Display a listing of the resource.
     *  Password - DS123perfect
        Username - debarati
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = LocationMap::orderBy('created_at', 'desc')->get();
        return view('locationmap.index',compact('images'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image'=>'required|mimes:jpeg,jpg,png|max:5242880',
        ]);

        $lovation_map = new LocationMap;
        if(isset($request->image) && !empty($request->image)){
            $image = $request->image->store('public/locationmap');
            $image = str_replace("public", "storage", $image);
            $lovation_map->image = $image;
        }
        $lovation_map->save();
        return response(['message'=>"Image stored successfully","data"=>(object)[],"errors"=>array("exception"=>["Everything is OK."])],201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
