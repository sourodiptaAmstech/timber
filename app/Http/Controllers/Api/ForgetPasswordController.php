<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Laravel\Passport\Events\AccessTokenCreated;
use Laravel\Passport\Client;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class ForgetPasswordController extends Controller
{
    public function forgetPassword(Request $request){
        try{
            $request->validate(['email'=>'email|required']);
            $user=User::where("email",$request->email)->first();
            if(empty($user)){
                return response([
                    'success' => false,
                    'message'=>"Couldn't find your Timber Account",
                    "errors"=>array("email"=>["Couldn't find your Timber Account"])
                ],404);
            }
            $password = $this->generatedPassword();
          
            $this->RevokeAccess($user->id);
            $user->password = Hash::make($password);
            $user->save();
            $emailData=array(
                "lineOne"=>"You are receiving this email because we received a password reset request for your account. Your login credentials as follow",
                "lineTwo"=>"Please use this new password to login into your account in future."
            );
            $this->SendCredentialByEmail($request->email,$password,$emailData);
            return response([
                'success' => true,
                'message'=>"OTP sent to your email id",
                "errors"=>(object)array()
            ],201);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }
    }
}
