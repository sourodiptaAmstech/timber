<?php 

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Validator;
use App\model\printLogs\PrintLogs;
use App\model\timber\Timber;
use App\model\stock\Stock;
use App\model\location\Location;
use PDF;


class PrintLogsController extends Controller 
{

  public function index()
  {
    $id=Auth::user()->id;
    $PrintLogs = PrintLogs::join('stock','print_logs.stock_id', '=', 'stock.stock_id')
      ->join('timber','print_logs.timber_id','=','timber.timber_id')
      ->join('location','location.location_id','=','stock.location_id')
      ->select('print_logs.*','timber.number as timber_number','location.number as location_number')
      ->where('print_logs.user_id',$id)->orderBy('print_logs.created_at', 'desc')->get();
  
    return response(['message'=>"Print Logs","data"=>(object)['printLogs' => $PrintLogs],"errors"=>array("exception"=>["Everything is OK."])],201);
  }


  public function generatePDF(Request $request,$id,$barcode)
  {
      $timbers = Timber::join('stock', 'timber.timber_id', '=', 'stock.timber_id')
        ->join('location', 'stock.location_id', '=', 'location.location_id')
        ->select('timber.*','location.number as location_number','stock.stock_id as stock_id')
        ->where('stock.deleted_at', Null)
        ->where('timber.bar_code', $barcode)
        ->orderBy('timber_id', 'desc')->first();

      if(isset($timbers)){
        view()->share('timbers',$timbers);
        
        $pdf = PDF::loadView('print');
        
        $printLogs = new PrintLogs;
        $printLogs->timber_id = $timbers->timber_id;
        $printLogs->user_id = $id;
        $printLogs->stock_id = $timbers->stock_id;
        $printLogs->save();

      return $pdf->download('timber.pdf');

    } else {

      return response(['success' => false, 'message'=>"Timber not found","data"=>(object)array(),"errors"=>(object)array("Not_Found"=>["Timber not found"])],404);
    }

  }
 
  public function create()
  {

  }

  
  public function store(Request $request)
  { 
   
  }

  
  public function show($id)
  {
    
  }

  
  public function edit($id)
  {
    
  }

  
  public function update($id)
  {
    
  }

  public function destroy($id)
  {
    
  }
  
}

?>