<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Validator;
use App\model\timber\Timber;
use App\model\stock\Stock;
use App\model\movement\Movement;
use App\Http\Controllers\Admin\TimberWebController;


class TimberController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {

  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {

  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {

    $timberObj = new TimberWebController;
    $timber = $timberObj->store($request);
    
    if ($timber->original['success'] == false) {
        return response()->json([
            'success' => false,
            'message'=> $timber->original['message']
        ],422);
    }
     
    if ($timber->original['success'] == true) {
        return response()->json([
            'success' => true,
            'message' => 'Timber Added Successfully',
            'data' => $timber->original['data']
        ],200);
    }

  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
      try
      {
          // 'location.name as location_name',
              $timber_id=$id;
              if((int)$timber_id==0){
                return response(['message'=>"The given data was invalid.","errors"=>(object)array("Timber ID"=>["Parameter is reqired"])],422);
              }
              $timbers = Timber::join('stock', 'timber.timber_id', '=', 'stock.timber_id')
              ->join('location', 'stock.location_id', '=', 'location.location_id')
              ->select('timber.*','location.number as location_number','location.location_id as location_id')
              ->where('timber.timber_id',$timber_id)
              ->orderBy('timber_id', 'desc')->get()->toArray();
              if(!empty($timbers))
              return response(['success' => true, 'message'=>"Timber found","data"=>(object)$timbers[0],"error"=>(object)array() ],200);
              else
              return response(['success' => false, 'message'=>"Timber not found","data"=>(object)array(),"errors"=>(object)array("Not_Found"=>["Timber not found"])],404);
            }
            catch(\Illuminate\Database\QueryException  $e){
                return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
                // return response(['Exception'=>$e],400 );
            }
            catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
                return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
                //return response(['Exception'=>$e],400 );
            }
    }
        /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {

  }

  public function timberMovement(Request $request)
  {

    $user_id = Auth::id();
    $datas = $request->data;
    foreach ($datas as $key => $value) {

      $stock = Stock::where('timber_id', $value['timber_id'])->first();
      $stock->location_id = $value['location_id'];
      $stock->save();

      $movement = new Movement;
      $movement->stock_id = $stock->stock_id;
      $movement->location_id = $stock->location_id;
      $movement->user_id =  $user_id;
      $movement->save();
    }
    return response()->json(['success' => true,'message' => 'Timbers moved successfully',"data"=>[],"error"=>(object)array() ],200);
  }

  public function timberMassMovement(Request $request)
  {

    $user_id = Auth::id();
    $location_id = $request->location_id;
    $timber_ids = $request->timber_id_list;
    foreach ($timber_ids as $timber_id) {
      $stock = Stock::where('timber_id', $timber_id)->first();
      $stock->location_id = $location_id;
      $stock->save();

      $movement = new Movement;
      $movement->stock_id = $stock->stock_id;
      $movement->location_id = $stock->location_id;
      $movement->user_id =  $user_id;
      $movement->save();
    }
    return response()->json(['success' => true,'message' => 'Timbers moved successfully',"data"=>[],"error"=>(object)array() ],200);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {

  }


  public function search(Request $request)
  {
    try{
      $validatedData=$request->validate([
          'searchQueary'=>'required',
          'page'=>'required|integer'
      ]);
      $searchQueary=$request->searchQueary;
      $pageNo=$request->pages;
      $limt=25;
      $timbers = Timber::join('stock', 'timber.timber_id', '=', 'stock.timber_id')
                ->join('location', 'stock.location_id', '=', 'location.location_id')
                ->select('timber.*', 'location.number as location_number','location.location_id as location_id')
                //->orwhere('timber.number', 'LIKE', '%'.$searchQueary.'%')
               // ->orwhere('timber.name', 'LIKE', '%'.$searchQueary.'%')
               // ->orwhere('location.number', 'LIKE', '%'.$searchQueary.'%')
                ->where('stock.deleted_at', null)
                ->where(function($q)  use ($searchQueary){
                    $q->orwhere('timber.number', $searchQueary)
                    // ->orwhere('timber.name', $searchQueary)
                    ->orwhere('location.number',$searchQueary);
                    // ->orwhere('location.name',$searchQueary);
                })
                ->orderBy('timber_id', 'desc')->get()->toArray();

      if(!empty($timbers)){
        return response(['success' => true, 'message'=>"Timber found","data"=>(array)$timbers,"error"=>(object)array() ],200);
      } else {
        return response(['success' => false, 'message'=>"Timber not found","data"=>array(),"errors"=>(object)array("Not_Found"=>["Timber not found"])],404);
      }

    }
    catch(\Illuminate\Database\QueryException  $e){
      return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
      // return response(['Exception'=>$e],400 );
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
      return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
      //return response(['Exception'=>$e],400 );
    }

  }

  public function timberByLocation($id){
    try{
    $location_id=$id;
    if((int)$location_id==0){
      return response(['message'=>"The given data was invalid.","errors"=>(object)array("Location ID"=>["Parameter is reqired"])],422);
    }
    $timbers = Timber::join('stock', 'timber.timber_id', '=', 'stock.timber_id')
    ->join('location', 'stock.location_id', '=', 'location.location_id')
    ->select('timber.*','location.number as location_number')
    ->where('stock.deleted_at', Null)
    ->where('location.location_id',$location_id)
    ->orderBy('timber_id', 'desc')->get()->toArray();
    if(!empty($timbers)){
      return response(['success' => true, 'message'=>"Timber found","data"=>(array)$timbers,"error"=>(object)array() ],200);
    }else{
      return response(['success' => false, 'message'=>"Timber not found","data"=>[],"errors"=>(object)array("Not_Found"=>["Timber not found"])],404);
    }
  }
  catch(\Illuminate\Database\QueryException  $e){
      return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
      // return response(['Exception'=>$e],400 );
  }
  catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
      return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
      //return response(['Exception'=>$e],400 );
  }
  }

  public function searchByBarcode(Request $request)
  {
    try{
      $validatedData=$request->validate([
        'bar_code'=>'required',
      ]);
      $bar_code=$request->bar_code;
      $timbers = Timber::join('stock', 'timber.timber_id', '=', 'stock.timber_id')
      ->join('location', 'stock.location_id', '=', 'location.location_id')
      ->select('timber.*', 'location.number as location_number','location.location_id as location_id','stock.stock_id as stock_id')
      ->where('stock.deleted_at', Null)
      ->where('timber.bar_code', $bar_code)
      ->orderBy('timber_id', 'desc')->first();
      
      if(!empty($timbers))
        return response(['success' => true, 'message'=>"Timber found","data"=>$timbers,"error"=>(object)array() ],200);
      else
        return response(['success' => false, 'message'=>"Timber not found","data"=>[],"errors"=>(object)array("Not_Found"=>["Timber not found"])],404);
      }

    catch(\Illuminate\Database\QueryException  $e){
        return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
    }

    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
    }
    
  }

}

?>
