<?php


namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Validator;
use App\model\location\Location;

class LocationController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index(Request $request)
  {
      $location=Location::where("active",1)->orderBy("number")->get()->toArray();
      if(!empty($location))
        return response(['success' => true, 'message'=>"Location found successfully","data"=>(array)$location,"error"=>(object)array() ],200);
      else
        return response(['success' => false, 'message'=>"Location not available","data"=>[],"errors"=>(object)array("Not_Found"=>["Location not available"])],404);

  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {

  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    $validatedData=$request->validate([
        // 'name'=>'required',numeric
        'number'=>'required|alpha_num|unique:location,number'
    ]);
    $location = Location::create($validatedData);
    return response([
        'message'=>"Location added successfully",
        "errors"=>(object)array(),
        "location"=>$location
      ],201);

  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {


  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {

  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {

  }

}

?>
