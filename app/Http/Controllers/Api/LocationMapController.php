<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\locationmap\LocationMap;
use URL;

class LocationMapController extends Controller
{
    public function getLocationMapImage()
    {
    	$images = LocationMap::orderBy('created_at','desc')->get();
    	foreach ($images as $key => $image) {
    		$image->image=URL::asset($image->image);
    	}
    	return response(['message'=>"Location map images","data"=>(object)["locationmap"=>$images],"errors"=>array("exception"=>["Everything is OK."])],201);
    }
}
