<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use URL;

class AuthController extends Controller
{
    public function register(Request $request){
        try{
            $validatedData=$request->validate([
                'first_name'=>'required|max:55',
                'last_name'=>'required|max:55',
                'position'=>'required|integer|max:20',
                'email'=>'email|required|unique:users',
               // 'password'=>'required|confirmed',
                'account_type'=>'required|integer|max:4'
            ]);
            //"password":"password123#",
            //"password_confirmation":"password123#",
            $password=$this->generatedPassword();
            $validatedData['password']=Hash::make($password);
            $user=User::create($validatedData);
            //$user->createToken('authToken')->accessToken;
            // send email to the user with user credential.
            $emailData=array(
                "lineOne"=>"You are receiving this email because we received a new account creation request for Timber account. Your login credentials as follow",
                "lineTwo"=>"Please use the above credentials to login into your Timber account in future."
            );
            $this->SendCredentialByEmail($request->email,$password,$emailData);

            return response(['message'=>"User successfully registered ","errors"=>(object)array()],201);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
           // return response(['Exception'=>$e],400 );
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
            //return response(['Exception'=>$e],400 );
        }
    }

    public function login(Request $request){
        try{
            $request->validate([
                'email'=>'email|required',
                'password'=>'required',
            ]);
            if(!Auth::attempt(['email' => $request->email,'password' => $request->password,'account_type' => 1,'status' => 1])){
                return response(['messgae'=>'Invalid credentials',"errors"=>array("exception"=>["Invalid credentials"])],401);
            }

            $access_token=Auth::user()->createToken('authToken')->accessToken;

            if (!isset(Auth::user()->image)) {
                $image = 'images/NO_IMG.png';
            } else {
                $image = Auth::user()->image;
            }

            return response(['messgae'=>'Login successfully',
            "errors"=>(object)array(),
            'data'=>array(
                "access_token"=>$access_token,
                "token_type"=>'Bearer',
                "user_id"=>Auth::user()->id,
                "account_type"=>Auth::user()->account_type,
                "first_name"=>Auth::user()->first_name,
                "last_name"=>Auth::user()->last_name,
                "image"=>URL::asset($image),
                )],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }
    }

    public function uploadProfileImage(Request $request)
    {
        try{
            $id=Auth::user()->id;
            $request->validate([
                'image'=>'mimes:jpeg,jpg,png',
            ]);
            $user = User::find($id);
            if (!isset($user->image)) {
                $user->image = 'images/NO_IMG.png';
            }
            
            if(isset($request->image) && !empty($request->image)){
                $image = $request->image->store('public/user/image/'.$id);
                $image = str_replace("public", "storage", $image);
                $user->image=$image;
            }
            $user->save();
            return response(['message'=>"Thank you for uploading your profile image!","data"=>(object)["image"=>URL::asset($user->image)],"errors"=>array("exception"=>["Everything is OK."])],201);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>"User Not Found","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"])],403);
        }
    }

    public function storeDeviceToken(Request $request)
    {
        try {
            $request->validate([
                'user_id' => 'required',
                'device_token' => 'required'
            ]);
            
            $user = User::find($request->user_id);
            $user->device_token = $request->device_token;
            $user->save();
            return response(['message'=>"Device token inserted successfully","data"=>(object)[],"errors"=>array("exception"=>["Everything is OK."])],201);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }
    }

}