<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use App\Notifications\email;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected function generatedPassword(){
        $data = '123QWERTYUIOPASDFGHJKLZXCVBNM456mnbvcxzlkjhgfdsapoiuytrewq789!@#$%&*_0';
        return substr(str_shuffle($data), 0, 7);
    }
    protected function RevokeAccess($user_id){
        $revokeAccess=DB::table('oauth_access_tokens')->where('user_id', $user_id)->delete();
        return true;
    }
    protected function SendCredentialByEmail($emailID,$password,$emailData){
        Notification::route('mail', $emailID)->notify(new email($password,$emailData,$emailID));
        return true;
    }

}
