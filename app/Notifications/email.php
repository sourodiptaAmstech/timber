<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class email extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
      private $password;
      private $user_id;
      private $emailData;
    public function __construct($data,$emailData,$user_id)
    {
        //
        $this->password=$data;
        $this->user_id=$user_id;
        $this->emailData=$emailData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line($this->emailData['lineOne'])
            ->line("User ID: ".$this->user_id)
            ->line("Password: ".$this->password)
            ->line($this->emailData['lineTwo']);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
