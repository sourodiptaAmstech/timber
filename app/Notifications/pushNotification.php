<?php

namespace App\Notifications;

use Illuminate\Support\Facades\Log;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class PushNotification
{
    public static function PushNotifications($fcmDetails)
    {
        try {
            $dataValue     = $fcmDetails['data'];
            $notificationBuilder = new PayloadNotificationBuilder($fcmDetails['notification']['title']);
            $notificationBuilder->setBody($fcmDetails['notification']['body'])->setSound('default');
            $notification  = $notificationBuilder->build();
            $dataBuilder   = new PayloadDataBuilder();
            $dataBuilder->addData($dataValue);
            $data          = $dataBuilder->build();
            $optionBuilder = new OptionsBuilder();
            $option        = $optionBuilder->build();
            $token         = $fcmDetails['to'];

            if(isset($token['Android'])){
                $downstreamResponse = FCM::sendTo($token['Android'], $option, $notification, $data);
            }

            if(isset($token['IOS'])){
                $downstreamResponse = FCM::sendTo($token['IOS'], $option, $notification, $data);
            }
            Log::info("Date Time IN PushNotifications :".date("Y-m-d H:i"));
            Log::info($fcmDetails);
            Log::info("file PushNotification Line 36");

            if (!empty($downstreamResponse)) {
                Log::info("push send : true");
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            return $e->GetMessage();
        }
    }
}
