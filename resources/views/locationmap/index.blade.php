@extends('layouts.layout')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class=""><h4 class="card-title">Location Map</h4></div>
                <br>
                <div class="alert alert-danger image-error col-lg-8" role="alert" style="display:none;">
                    <p class="mesg-error"></p>
                </div>
                <form  method="post" id="upload_form" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                    <label for="name" class="form-label text-dark">Upload Image</label>
                    <input type="file" id="image" name="image" class="form-control col-lg-8" required>
                    </div>
                    <input type="submit" class="btn btn-info" value="Submit">
                </form>
                <br>
                <table id="locationmap_table" class="table-responsive table table-bordered table-striped table-hover">
                <thead class="bg-color">
                    <tr>
                        <th>S.No</th>
                        <th>Image</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($images as $index=>$image)
                    <tr>
                        <td>{{$index+1}}</td>
                       <td>
                           @if(isset($image->image))
                                @if(File::exists(storage_path('app/public' .str_replace("storage", "", $image->image))))
                                <img src="{{URL::asset($image->image)}}" style="height: 200px; width: 400px;">
                                @else
                                <img src="{{URL::asset('images/NO_IMG.png')}}" style="height: 50px; width: 65px;">
                                @endif
                            @else
                                <img src="{{URL::asset('images/NO_IMG.png')}}" style="height: 50px; width: 65px;">
                            @endif
                       </td>
                    </tr>
                    @endforeach
                </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
<style>
    .col-sm-12.col-md-6 {
        margin-bottom: -20px;
    }
</style>
@endsection
@section('scripts')
<script>
    $(document).ready(function () {

        $('#locationmap_table').DataTable();

        $('#upload_form').on('submit', function(event){
            event.preventDefault();
            $.ajax({
            url:"{{ route('admin.location-map.store') }}",
            method:"POST",
            data:new FormData(this),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
            success:function(data){
                alert('Image Uploaded successfully');
                window.location.href = '{{ route('admin.location-map.index') }}';
            },
            error: function(response){
                if (response.status == 422){
                    $('.image-error').show();
                    $('.mesg-error').html(response.responseJSON.errors.image);
                }
                if (response.status == 500){
                    $('.image-error').show();
                    $('.mesg-error').text('Internal server error');
                }
            }
          });
        });
    });
</script>
@endsection