@extends('layouts.layout')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class=""> <h4 class="card-title">Edit Position</h4></div>
                <br>
                    <div class="alert alert-danger position-edit-error col-lg-8" role="alert" style="display:none;">
                        <p class="mesg-error"></p>
                    </div>
                <form>
                    <div class="form-group">
                        <label for="name" class="form-label text-dark">Position</label>
                        <input type="text" id="name" name="name" class="form-control col-lg-8" value="{{$position->name}}" placeholder="Position Name" required>
                    </div>

                    <div class="form-group">
                    <label for="active" class="form-label text-dark">Status</label>

                    <select name="active" id="active" class="form-control col-lg-8" required>
                        <option value="">Select</option>
                        <option value="1" {{ $position->active == 1 ? 'selected' : '' }}>Active</option>
                        <option value="0" {{ $position->active == 0 ? 'selected' : '' }}>Inactive</option>
                    </select>
                    </div>
                    
                    <a href="javascript:void(0)" class="btn btn-info float-left" data-id="{{$position->id}}" id="editUser">{{ trans('global.update') }}</a>&nbsp;
                    <a href="{{ route('admin.positions.index') }}" style="border: 1px solid #808890;" class="btn btn-outline-default">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){

        $('#editUser').click(function(e){
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var name = $('#name').val();
            var active = $('#active').val();            
            var id = $(this).data("id");

            $.ajax({
                url:"{{ route("admin.positions.update",[0]) }}",
                type: 'PUT',
                data: { id: id, name: name, active: active },
                success: function(response){
                    
                    if (response.success) {
                        alert('Position Updated Successfully')
                        window.location.href = '{{ route('admin.positions.index') }}'
                    }
                },

                error: function(response){
                    if (response.status == 422){
                        $('.position-edit-error').show(0).delay(3000).hide(400);
                        setTimeout(function() { 
                            $('.position-edit-error').fadeOut(); 
                        }, 3000);
                        $('.mesg-error').html(response.responseJSON.message.active);
                        $('.mesg-error').html(response.responseJSON.message.name);
                    }
                }
            });
        });
    });

</script>
@endsection
