@extends('layouts.layout')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class=""><h4 class="card-title">Add Position</h4></div>
                <br>
                <div class="alert alert-danger position-error col-lg-8" role="alert" style="display:none;">
                    <p class="mesg-error"></p>
                </div>
                <form>
                    <div class="form-group">
                    <label for="name" class="form-label text-dark">Position</label>
                    <input type="text" id="name" name="name" class="form-control col-lg-8" placeholder="Position Name" required>
                    <span class="name-span"></span>
                    </div>
                    <a href="javascript:void(0)" class="btn btn-info" id="storeUser">{{ trans('global.save') }}</a>
                </form>
          
            <table id="position_table" class="table-responsive table table-bordered table-striped table-hover">
                <thead class="bg-color">
                    <tr>
                        <th>Position</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($positions as $position)
                    <tr>
                        <td>{{$position->name}}</td>

                        @if ($position->active == 1)
                        <td><a class="positionStatus btn btn-success btn-xs" data-id="{{$position->id}}" href="javascript:void(0)">Active</a></td>
                        @else
                        <td><a class="positionStatus btn btn-danger btn-xs" data-id="{{$position->id}}" href="javascript:void(0)">Inactive</a></td>
                        @endif
                        <td>
                            <a href="{{ route('admin.positions.edit', [$position->id]) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit</a>

                            <a class="btn btn-danger deleteRecord btn-xs" data-id="{{$position->id}}" href="javascript:void(0)" method='delete'><i class="fa fa-trash"></i> Delete</a>

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .name {
        border: 1px solid red;
    }
    .col-sm-12.col-md-6 {
        margin-bottom: -20px;
    }
</style>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        $('#position_table').DataTable({
            'columnDefs': [{
                'targets': [2],
                'orderable': false
            }],
            'sPaginationType': 'full_numbers'
            
        });

        // $('input#name').keydown(function(){
        //     $('#name').removeClass('name');
        //     $('span.name-span').empty();
        // });

        $('#storeUser').click(function(e){
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var name = $('#name').val();
           
            $.ajax({
                url: "{{ route('admin.positions.store') }}",
                method: 'post',
                data: { name: name },

                success: function(response){
                    if (response.success) {
                        alert('Position added successfully')
                        location.reload()
                    }
                },

                error: function(response){
                    if (response.status == 422){
                        $('.position-error').show(0).delay(3000).hide(400);
                        setTimeout(function() { 
                            $('.position-error').fadeOut(); 
                        }, 3000);
                        $('.mesg-error').html(response.responseJSON.message.name);
                        // $('#name').addClass('name');
                        // $('.name-span').text(response.responseJSON.message.name).css({'color':'red'});
                    }
                }
            });
        });

        $(document).on('click','.deleteRecord', function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var id = $(this).data("id");

            if (confirm('Are you sure to Delete?')) {
                $.ajax({
                    url: "positions/"+id,
                    type: 'DELETE',
                    data: {
                        "id": id,
                    },
                    
                    success: function (response){
                        location.reload();
                    },
                    error: function(response){
                        if (response.status == 404){
                            $('.position-error').show(0).delay(3000).hide(400);
                            setTimeout(function() { 
                                $('.position-error').fadeOut(); 
                            }, 3000);
                            $('.mesg-error').html(response.responseJSON.message);
                     }
                 }

             });
            }

        })


        $(document).on('click','.positionStatus', function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var id = $(this).data("id");

            if (confirm('Are you sure to change status?')) {
                $.ajax({
                    url: "positions/active/"+id,
                    type: 'post',
                    data: {
                            "id": id
                        },
                    success: function (response){
                        location.reload();
                    }
                });
            }
        });
      
    });

</script>
@endsection
