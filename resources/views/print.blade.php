<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Timber</title>
    <style type="text/css">
        body {
            background-color: #fff;
            margin: 40px;
            font: 13px/20px normal Helvetica, Arial, sans-serif;
            color: #4F5155;
        }
        h1 {
            color: #444;
            background-color: transparent;
            border-bottom: 1px solid #D0D0D0;
            font-size: 19px;
            font-weight: normal;
            margin: 0 0 14px 0;
            padding: 14px 15px 10px 15px;
        }
        #container {
            margin: 10px;
            border: 1px solid #D0D0D0;
            text-align: center;
        }
    </style>
</head>
<body>
    <div id="container">
            @php
            $timber="Timber number: ".$timbers->number.", Barcode: ".$timbers->bar_code.", Location number: ".$timbers->location_number.", Created datetime: ".date('d M Y h:i:s A',strtotime($timbers->created_at))."";
            @endphp
        <div style="text-align: center;">
            <div>
                {{$timbers->number}}
                
            </div><br>
            <div>
                <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(140)->generate($timber)) !!}">
            </div><br>
        </div>
    </div>
</body>
</html>