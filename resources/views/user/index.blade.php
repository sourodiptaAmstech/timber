@extends('layouts.layout')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">

                <div class="card-title"> <h4>User List</h4></div><br>
                
                <table id="user_table" class="table-responsive table table-bordered table-striped table-hover">
                    <thead class="bg-color">
                        <tr>
                            <th>Fist Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Position</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td>{{$user->first_name}}</td>
                            <td>{{$user->last_name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->positions->name}}</td>
                            <td>
                                @if ($user->status == 1)
                                    <a class="blockRecord btn btn-success btn-xs" data-id="{{$user->id}}" href="javascript:void(0)">Unblock</a>
                                @else
                                    <a class="blockRecord btn btn-danger btn-xs" data-id="{{$user->id}}" href="javascript:void(0)">Block</a>
                                @endif

                               {{--  <a class="deleteRecord fa fa-trash" data-id="{{$user->id}}" href="#" data-toggle="tooltip" data-placement="top" title="Delete" method='delete'></a> --}}

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .toolbar {
    float: left;
}
</style>

@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        $('#user_table').DataTable({
            'columnDefs': [{
                'targets': [4],
                'orderable': false
            }],
            'dom': 'l<"toolbar">frtip',
            'initComplete': function(){
                $("div.toolbar").html('<a class = "btn btn-sm btn-info" href="{{ route('admin.users.create') }}"><i class="fa fa-plus"></i> Add User</a>');
            },
            "sPaginationType": "full_numbers"
        });

    //.....User Delete..........................................................
        // $(document).on('click','.deleteRecord', function(){
        //     $.ajaxSetup({
        //         headers: {
        //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //         }
        //     });
        //     var id = $(this).data("id");
        //    // var token = $("meta[name='csrf-token']").attr("content");

        //     if (confirm('Are you sure to Delete?')) {
        //         $.ajax({
        //             url: "users/"+id,
        //             type: 'DELETE',
        //             data: {
        //                 "id": id,
        //                 //"_token": token,
        //             },
        //             success: function (response){
        //                 location.reload();
        //                 // if (response.success) {
        //                 //     $('.alert-success').show();
        //                 //     $('.mesg-success').html(response.message);
        //                 // }
        //             }
        //         });
        //     }
        // });
    //........End Delete..........................................................

        $(document).on('click','.blockRecord', function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var id = $(this).data("id");

            if (confirm('Are you sure to change status?')) {
                $.ajax({
                    url: "users/status/"+id,
                    type: 'post',
                    data: {
                            "id": id
                        },
                    success: function (response){
                        location.reload();
                    }
                });
            }
        });

    });
</script>
@endsection
