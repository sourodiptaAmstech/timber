@extends('layouts.layout')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class=""> <h4 class="card-title">Add User</h4></div>
                    <br>
                    <div class="alert alert-danger user-error col-lg-8" role="alert" style="display:none">
                        <p class="mesg-error" style="color: red;"></p>
                    </div>

                <form id="validateForm">
                    <div class="form-group">
                    <label for="first_name" class="form-label text-dark">First Name</label>
                    <input type="text" id="first_name" name="first_name" class="form-control col-lg-8" placeholder="First name" required>
                    </div>

                    <div class="form-group">
                    <label for="last_name" class="form-label text-dark">Last Name</label>
                    <input type="text" id="last_name" name="last_name" class="form-control col-lg-8" placeholder="Last name" required>
                    </div>

                    <div class="form-group">
                    <label for="email" class="form-label text-dark">Email</label>
                    <input type="email" id="email" name="email" class="form-control col-lg-8" placeholder="Email" required>
                    </div>

                    <div class="form-group">
                    <label for="position" class="form-label text-dark">Position</label>

                    <select name="position" id="position" class="form-control col-lg-8" required>
                        <option value="">Select</option>
                        @foreach($positions as $position)
                            <option value="{{$position->id}}" >{{ $position->name }}</option>
                        @endforeach
                    </select>
                    </div>
                    
                    <a href="javascript:void(0)" class="btn btn-info float-left save_button" id="storeUser">{{ trans('global.save') }}</a>
                    <button class="btn btn-info float-left" style="display: none;" id="load_button">Saving <i class='fa fa-circle-o-notch fa-spin'></i></button>&nbsp;
                    <a href="{{ route('admin.users.index') }}" style="border: 1px solid #808890;" class="btn btn-outline-default">Cancel</a>
                   
                </form>

            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script>
    $(document).ready(function(){

        $('#storeUser').click(function(e){
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var first_name = $('#first_name').val();
            var last_name = $('#last_name').val();
            var email = $('#email').val();
            var position = $('#position').val();
            $('.save_button').css({display:'none'});
            $('#load_button').css({display:'block'});
            $.ajax({
                url: "{{ route('admin.users.store') }}",
                method: 'post',
                data: { first_name: first_name, last_name: last_name, email: email, position: position },

                success: function(response){
                    $('#load_button').css({display:'none'});
                    $('.save_button').css({display:'block'});
                    if (response.success) {
                        alert('User created successfully')
                        window.location.href = '{{ route('admin.users.index') }}'
                    }
                },

                error: function(response){
                    $('#load_button').css({display:'none'});
                    $('.save_button').css({display:'block'});
                    if (response.status == 422){
                        $('.user-error').show(0).delay(5000).hide(400);
                        setTimeout(function() { 
                            $('.user-error').fadeOut(); 
                        }, 5000);
                        $('.mesg-error').html(response.responseJSON.errors.position);
                        $('.mesg-error').html(response.responseJSON.errors.email[0]);
                        $('.mesg-error').html(response.responseJSON.errors.email[1]);
                        $('.mesg-error').html(response.responseJSON.errors.last_name);
                        $('.mesg-error').html(response.responseJSON.errors.first_name);
                    }
                    if (response.status == 500){
                        $('.user-error').show(0).delay(3000).hide(400);
                        setTimeout(function() { 
                            $('.user-error').fadeOut(); 
                        }, 3000);
                        $('.mesg-error').text('Internal server error');
                    }
                }
            });
        });
    });

</script>
@endsection
