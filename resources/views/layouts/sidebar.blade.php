  <div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures" style="height: 100%; position: fixed;">
            <div class="nano">
                <div class="nano-content">
                     <div class="logo"><img src="{{ asset('images/Logo.png') }}" alt=""/></div>
                        
                    <ul>
                        <li class="{{ request()->is('admin/home') || request()->is('admin/home/*') ? 'active' : '' }}">
                        <a href="{{ route('admin.home') }}"><i class="ti-home"></i> Dashboard </a>   
                        </li>

                        <li class="{{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                            <a href="{{ route('admin.users.index') }}" ><i class="ti-user"></i> User </a>
                        </li>

                        <li class="{{ request()->is('admin/positions') || request()->is('admin/positions/*') ? 'active' : '' }}">
                            <a href="{{ route('admin.positions.index') }}"><i class="ti-layout"></i> Position </a>
                        </li>

                        <li class="{{ request()->is('admin/timbers') || request()->is('admin/timbers/*') ? 'active' : '' }}">
                        <a href="{{ route('admin.timbers.index') }}"><i class="ti-view-list-alt"></i>Timber</a>
                        </li>

                        <li class="{{ request()->is('admin/location-map') || request()->is('admin/location-map/*') ? 'active' : '' }}">
                            <a href="{{ route('admin.location-map.index') }}"><i class="ti-layout"></i> Location map </a>
                        </li>

                        <li class="{{ request()->is('admin/printlogs') || request()->is('admin/printlogs/*') ? 'active' : '' }}">
                        <a href="{{ route('admin.printlogs.index') }}"><i class="ti-view-list-alt"></i>Print logs</a>
                        </li>
                    

                        {{-- <li><a href="app-event-calender.html"><i class="ti-calendar"></i> Calender </a></li>

                        <li class="label">Apps</li>
                        <li><a class="sidebar-sub-toggle"><i class="ti-bar-chart-alt"></i>  Charts  <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                                <li><a href="chart-flot.html">Flot</a></li>
                                <li><a href="chart-morris.html">Morris</a></li>
                                <li><a href="chartjs.html">Chartjs</a></li>
                                <li><a href="chartist.html">Chartist</a></li>
                                <li><a href="chart-peity.html">Peity</a></li>
                                <li><a href="chart-sparkline.html">Sparkle</a></li>
                                <li><a href="chart-knob.html">Knob</a></li>
                            </ul>
                        </li>

                        <li><a href="app-email.html"><i class="ti-email"></i> Email</a></li>
                        <li><a href="app-profile.html"><i class="ti-user"></i> Profile</a></li>
                        <li><a href="app-widget-card.html"><i class="ti-layout-grid2-alt"></i> Widget</a></li>
                        <li class="label">Features</li>
                        <li><a class="sidebar-sub-toggle"><i class="ti-layout"></i> UI Elements <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                                <li><a href="ui-typography.html">Typography</a></li>
                                <li><a href="ui-alerts.html">Alerts</a></li>

                                <li><a href="ui-button.html">Button</a></li>
                                <li><a href="ui-dropdown.html">Dropdown</a></li>

                                <li><a href="ui-list-group.html">List Group</a></li>

                                <li><a href="ui-progressbar.html">Progressbar</a></li>
                                <li><a href="ui-tab.html">Tab</a></li>

                            </ul>
                        </li>
                        <li><a class="sidebar-sub-toggle"><i class="ti-panel"></i> Components <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                                <li><a href="uc-calendar.html">Calendar</a></li>
                                <li><a href="uc-carousel.html">Carousel</a></li>
                                <li><a href="uc-weather.html">Weather</a></li>
                                <li><a href="uc-datamap.html">Datamap</a></li>
                                <li><a href="uc-todo-list.html">To do</a></li>
                                <li><a href="uc-scrollable.html">Scrollable</a></li>
                                <li><a href="uc-sweetalert.html">Sweet Alert</a></li>
                                <li><a href="uc-toastr.html">Toastr</a></li>
                                <li><a href="uc-range-slider-basic.html">Basic Range Slider</a></li>
                                <li><a href="uc-range-slider-advance.html">Advance Range Slider</a></li>
                                <li><a href="uc-nestable.html">Nestable</a></li>

                                <li><a href="uc-rating-bar-rating.html">Bar Rating</a></li>
                                <li><a href="uc-rating-jRate.html">jRate</a></li>
                            </ul>
                        </li>
                        <li><a class="sidebar-sub-toggle"><i class="ti-layout-grid4-alt"></i> Table <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                                <li><a href="table-basic.html">Basic</a></li>

                                <li><a href="table-export.html">Datatable Export</a></li>
                                <li><a href="table-row-select.html">Datatable Row Select</a></li>
                                <li><a href="table-jsgrid.html">Editable </a></li>
                            </ul>
                        </li>
                        <li><a class="sidebar-sub-toggle"><i class="ti-heart"></i> Icons <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                                <li><a href="font-themify.html">Themify</a></li>
                            </ul>
                        </li>
                        <li><a class="sidebar-sub-toggle"><i class="ti-map"></i> Maps <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                                <li><a href="gmaps.html">Basic</a></li>
                                <li><a href="vector-map.html">Vector Map</a></li>
                            </ul>
                        </li>
                        <li class="label">Form</li>
                        <li class="label">Extra</li>
                        <li><a class="sidebar-sub-toggle"><i class="ti-files"></i> Invoice <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                                <li><a href="invoice.html">Basic</a></li>
                                <li><a href="invoice-editable.html">Editable</a></li>
                            </ul>
                        </li>
                        <li><a class="sidebar-sub-toggle"><i class="ti-target"></i> Pages <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                                <li><a href="page-login.html">Login</a></li>
                                <li><a href="page-register.html">Register</a></li>
                                <li><a href="page-reset-password.html">Forgot password</a></li>
                            </ul>
                        </li>
                        <li><a href="../documentation/index.html"><i class="ti-file"></i> Documentation</a></li> 

                        <li>
                            <a href="{{ route('admin.logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                                <i class="ti-close"></i>
                                Logout
                            </a>
                            <form id="frm-logout" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>

                        </li>  --}}
                    </ul>
                </div>
            </div>
        </div>
        <!-- /# sidebar -->