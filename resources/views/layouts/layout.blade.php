<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ trans('panel.site_title') }}</title>
{{-- Start template css --}}
        <!-- Standard -->
        <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
        <!-- Retina iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
        <!-- Retina iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
        <!-- Standard iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
        <!-- Standard iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

        <!-- Styles -->
        <link href="{{ asset('assets/css/lib/weather-icons.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/lib/owl.carousel.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/lib/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/lib/themify-icons.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/lib/menubar/sidebar.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/lib/bootstrap.min.css') }}" rel="stylesheet">

        <link href="{{ asset('assets/css/lib/helper.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet" />

{{-- End template css --}}
{{-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> --}}


    {{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> --}}
{{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet"/> --}}
    {{-- start --}}
    {{-- <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" /> --}}
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
    {{-- end --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" />    
    </head>

    <body>
      
        @include('layouts.sidebar')

        @include('layouts.header')


        <div class="content-wrap">
            <div class="main">
                <div class="container-fluid">
                   
                @yield('content')
                   
                </div>
            </div>
            <footer class="footer">
                <div class="container-fluid">
                  <div class="row text-xs-center">
                    <div class="col-sm-4 text-sm-left mb-0-5 mb-sm-0">
                        <p>
                        True North &copy; <script>document.write(new Date().getFullYear())</script>
                        </p>
                    </div>
                  </div>
                </div>
            </footer>
        </div>


    <!-- jquery vendor -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    {{-- ggg --}}
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    {{-- ggg --}}

{{-- Start template js --}}
        {{-- <script src="{{ asset('assets/js/lib/jquery.min.js') }}"></script> --}}
        <script src="{{ asset('assets/js/lib/jquery.nanoscroller.min.js') }}"></script>
        <!-- nano scroller -->
        <script src="{{ asset('assets/js/lib/menubar/sidebar.js') }}"></script>
        <script src="{{ asset('assets/js/lib/preloader/pace.min.js') }}"></script>
        <!-- sidebar -->
        <script src="{{ asset('assets/js/lib/bootstrap.min.js') }}"></script>

        <!-- bootstrap -->
        <script src="{{ asset('assets/js/lib/circle-progress/circle-progress.min.js') }}"></script>
        {{-- <script src="{{ asset('assets/js/lib/circle-progress/circle-progress-init.js') }}"></script> --}}

        {{-- <script src="{{ asset('assets/js/lib/morris-chart/raphael-min.js') }}"></script> --}}


        {{-- <script src="{{ asset('assets/js/lib/morris-chart/morris.js') }}"></script> --}}
        {{-- <script src="{{ asset('assets/js/lib/morris-chart/morris-init.js') }}"></script> --}}

        <!--  flot-chart js -->
        {{-- <script src="{{ asset('assets/js/lib/flot-chart/jquery.flot.js') }}"></script> --}}
        {{-- <script src="{{ asset('assets/js/lib/flot-chart/jquery.flot.resize.js') }}"></script> --}}
        {{-- <script src="{{ asset('assets/js/lib/flot-chart/flot-chart-init.js') }}"></script> --}}
        <!-- // flot-chart js -->


        {{-- <script src="{{ asset('assets/js/lib/vector-map/jquery.vmap.js') }}"></script> --}}
        <!-- scripit init-->
        {{-- <script src="{{ asset('assets/js/lib/vector-map/jquery.vmap.min.js') }}"></script> --}}
        <!-- scripit init-->

        <script src="{{ asset('assets/js/lib/vector-map/jquery.vmap.sampledata.js') }}"></script>

        <!-- scripit init-->
        {{-- <script src="{{ asset('assets/js/lib/vector-map/country/jquery.vmap.world.js') }}"></script> --}}
        <!-- scripit init-->
        {{-- <script src="{{ asset('assets/js/lib/vector-map/country/jquery.vmap.algeria.js') }}"></script> --}}
        <!-- scripit init-->
        {{-- <script src="{{ asset('assets/js/lib/vector-map/country/jquery.vmap.argentina.js') }}"></script> --}}
        <!-- scripit init-->
        {{-- <script src="{{ asset('assets/js/lib/vector-map/country/jquery.vmap.brazil.js') }}"></script> --}}
        <!-- scripit init-->
        {{-- <script src="{{ asset('assets/js/lib/vector-map/country/jquery.vmap.france.js') }}"></script> --}}
        <!-- scripit init-->
        {{-- <script src="{{ asset('assets/js/lib/vector-map/country/jquery.vmap.germany.js') }}"></script> --}}
        <!-- scripit init-->
        {{-- <script src="{{ asset('assets/js/lib/vector-map/country/jquery.vmap.greece.js') }}"></script> --}}
        <!-- scripit init-->
        {{-- <script src="{{ asset('assets/js/lib/vector-map/country/jquery.vmap.iran.js') }}"></script> --}}
        <!-- scripit init-->
        {{-- <script src="{{ asset('assets/js/lib/vector-map/country/jquery.vmap.iraq.js') }}"></script> --}}
        <!-- scripit init-->
        {{-- <script src="{{ asset('assets/js/lib/vector-map/country/jquery.vmap.russia.js') }}"></script> --}}
        <!-- scripit init-->
        {{-- <script src="{{ asset('assets/js/lib/vector-map/country/jquery.vmap.tunisia.js') }}"></script> --}}
        <!-- scripit init-->
        {{-- <script src="{{ asset('assets/js/lib/vector-map/country/jquery.vmap.europe.js') }}"></script> --}}
        <!-- scripit init-->
        {{-- <script src="{{ asset('assets/js/lib/vector-map/country/jquery.vmap.usa.js') }}"></script> --}}
        <!-- scripit init-->
        {{-- <script src="{{ asset('assets/js/lib/vector-map/vector.init.js') }}"></script> --}}

        {{-- <script src="{{ asset('assets/js/lib/weather/jquery.simpleWeather.min.js') }}"></script> --}}
        {{-- <script src="{{ asset('assets/js/lib/weather/weather-init.js') }}"></script> --}}
        {{-- <script src="{{ asset('assets/js/lib/owl-carousel/owl.carousel.min.js') }}"></script> --}}
        {{-- <script src="{{ asset('assets/js/lib/owl-carousel/owl.carousel-init.js') }}"></script> --}}
        <script src="{{ asset('assets/js/scripts.js') }}"></script>
{{-- End template js --}}

        <script>
            $(document).ready(function(){
                $('.sidebar-toggle').click(function(){
                    if ($('.sidebar-toggle').hasClass('is-active')){
                        $('img').hide();
                        $('.logo').addClass('admin-logo');
                    } else {
                        $('img').show();
                        $('.logo').removeClass('admin-logo');
                    }
                });

                $("#myBtn").click(function(){
                    $("#exampleModalCenter").modal();
                });

                $('.change-password').click(function(){
                    $('.password-fields').show();
                    $(this).hide();
                });
                $('.cancel-change-password').click(function(){
                    $('.password-fields').hide();
                    $('.change-password').show();
                });

            //................Change Password For Admin...........................
                $('#storePassword').click(function(e){
                    e.preventDefault();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    var new_password = $('#new_password').val();
                    var current_password = $('#current_password').val();
                    var confirm_new_password = $('#confirm_new_password').val();

                    $.ajax({
                        url: "{{ route('admin.change-password') }}",
                        method: 'post',
                        data: { new_password: new_password, current_password: current_password, confirm_new_password: confirm_new_password },

                        success: function(response){
                            console.log(response)
                            if (response.success) {
                                $('.success').show();
                                $('.password-success').html(response.message);
                            }
                        },

                        error: function(response){
                            console.log(response)

                            if (response.status == 422){
                                $('.danger').show(0).delay(3000).hide(400);
                                setTimeout(function() { 
                                    $('.danger').fadeOut(); 
                                }, 3000);
                                $('.password-error').html(response.responseJSON.errors.confirm_new_password);
                                $('.password-error').html(response.responseJSON.errors.new_password);
                                $('.password-error').html(response.responseJSON.errors.current_password);
                            }
                            if (response.status == 404){
                                $('.danger').show(0).delay(3000).hide(400);
                                setTimeout(function() { 
                                    $('.danger').fadeOut(); 
                                }, 3000);
                                $('.password-error').html(response.responseJSON.message);
                            }
                            if (response.status == 500){
                                $('.danger').show(0).delay(3000).hide(400);
                                setTimeout(function() { 
                                    $('.danger').fadeOut(); 
                                }, 3000);
                                $('.password-error').text('Internal server error');
                            }
                        }
                    });
                });
            //....................................................................
          });
        </script>

            @yield('scripts')
    </body>

</html>
