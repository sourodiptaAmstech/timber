@extends('layouts.layout')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">

                <div class="card-title"> <h4>Print Log List</h4></div><br>
                
                <table id="printlog_table" class="table-responsive table table-bordered table-striped table-hover">
                    <thead class="bg-color">
                        <tr>
                            <th>Timber Number</th>
                            <th>Location Number</th>
                            <th>Printed By</th>
                            <th>DateTime</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($PrintLogs as $PrintLog)
                        <tr>
                            <td>{{$PrintLog->timber_number}}</td>
                            <td>{{$PrintLog->location_number}}</td>
                            <td>{{$PrintLog->user->first_name}} {{$PrintLog->user->last_name}}</td>
                            <td>{{date('d M Y, h:i A',strtotime($PrintLog->created_at))}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<style>
    .col-sm-12.col-md-6 {
        margin-bottom: -20px;
    }
</style>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        $('#printlog_table').DataTable();
    });
</script>
@endsection