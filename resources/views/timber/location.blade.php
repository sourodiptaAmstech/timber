<p style='display: none;' id="show_modal"></p>
{{-- ..........Model................. --}}
<div class="modal fade" id="locationModel" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add Location</h5>
        <a href="javascript:void(0)" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="font-size: 20px;">&times;</span>
        </a>
      </div>
      <div class="modal-body">
      
        <div class="d-flex justify-content-center" >
            <div class="col-lg-12">
                <div class="alert alert-danger loc-danger" role="alert" style="display:none">
                    <p class="loc-error" style="color: red;"></p>
                </div>

                <form>
                    <div class="row">
                       {{--  <div class="col-lg-4">
                            <div class="form-group">
                                <label class="control-label text-dark">Location Name</label>
                                <input type="text" id="location_name" name="name" class="form-control" placeholder="Name">
                            </div>
                        </div> --}}
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="control-label text-dark">Location Number</label>
                                <input type="text" id="location_number" name="number" class="form-control" placeholder="Number">
                            </div>
                        </div>
                    
                        <div class="col-lg-4" style="margin-top: 36px;">
                            <a href="javascript:void(0)" class="btn btn-info btn-sm" id="storeLocation">{{ trans('global.save') }}</a>
                        </div>
                    </div>
                </form>
                
            </div>
        </div>

            <table id="location_table" class="table table-responsive table-bordered table-striped table-hover" style="width: 100%">
                <thead class="bg-color">
                    <tr>
                        {{-- <th>Location Name</th> --}}
                        <th>Location Number</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                  
                </tbody>
            </table>

      </div>

      <div class="modal-footer">
        <a href="javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
    div#location_table_length {
        padding-top: 12px;
    }
    .select2-container--classic .select2-selection--single .select2-selection__arrow {
        height: 39px;
    }
</style>
{{-- end model --}}

{{-- ..........Edit Modal................. --}}
<div class="modal fade" id="editLocationModel" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-md modal-dialog-centered" role="document">
    <div class="modal-content" style="background-color: lavender;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Edit Location</h5>
         <a href="javascript:void(0)" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="font-size: 20px;">&times;</span>
        </a>
      </div>
      <div class="modal-body">
      
        <div class="row d-flex justify-content-center" >
            <div class="col-lg-10">

                <div class="alert alert-danger update-danger" role="alert" style="display:none">
                    <p class="update-error" style="color: red;"></p>
                </div>

                <form>
                    <div class="row">
                        <input type="hidden" id="hidden_id">
                       {{--  <div class="col-lg-8">
                            <div class="form-group">
                                <label class="control-label text-dark">Location Name</label>
                                <input type="text" id="name_update" name="name_update" class="form-control" placeholder="Name">
                            </div>
                        </div> --}}
                    </div>
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label class="control-label text-dark">Location Number</label>
                                <input type="text" id="number_update" name="number_update" class="form-control" placeholder="Number">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8">
                            <a href="javascript:void(0)" class="btn btn-info btn-sm" id="updateLocation">{{ trans('global.update') }}</a>
                        </div>
                    </div>
                </form>
                
            </div>
        </div>
      </div>

      <div class="modal-footer">
        <a href="javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>
      </div>
    </div>
  </div>
</div>
{{-- end Edit Modal --}}

@section('script')
<script>

    $(document).ready(function(){

        $('select#location').select2({
            theme: 'classic'
        });

        var dataObject={"data":[]};

        $('#show_modal').click(function(e) {
            e.preventDefault();
            dataObject={"data":[]};
            $.ajax({
                url: '{{ route('admin.locations.index') }}',
                type: 'GET',
                datatype: 'json',
                contentType: 'application/json; charset=utf-8',
                cache: false,
                success: function(response){
                   
                    var objectLength=Object.keys(response.data).length;
                    for(var i=0; i<objectLength; i++){
                        dataObject.data.push(
                            [
                                // response.data[i].name,
                                response.data[i].number,
                                '<a data-id="'+response.data[i].location_id+'" class="editRecord editLocation btn btn-info btn-xs" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>'
                            ]);
                        }
                    
                    $('#location_table').DataTable().clear().destroy();
                    $('#location_table').DataTable({
                        'columnDefs': [{
                            'targets': [1],
                            'orderable': false
                        }],
                        'scrollY':        '37vh',
                        'scrollCollapse': true,
                        'sPaginationType': 'full_numbers',
                        'data': dataObject.data,
                    });
                },
                error: function(response){
                    console.log('error')
                    console.log(response)
                    $('#location_table').DataTable();
                    
                }
            });
        });

        $("select#location").change(function (e) {
            var val = this.value;
            if (val == 'Add') {
                $("#locationModel").modal('show');
                $('#show_modal').trigger('click');
            }
        });

        $(document).on('click', '.editLocation', function () {
           
            $("#editLocationModel").modal('show');
            var id = $(this).data('id');
           
            $.ajax({
                url:"{{ route("admin.locations.edit",[0]) }}",
                type:'GET',
                data: {'id': id},
                dataType: 'json', 
                success:function(response){
                    console.log(response)
                    // $('#name_update').val(response.location.name);
                    $('#number_update').val(response.location.number);
                    $('#hidden_id').val(response.location.location_id);
                }
            });
        });


        $('#updateLocation').click(function(e){
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // var name = $('#name_update').val();
            var number = $('#number_update').val(); 
            var id = $('#hidden_id').val();

            $.ajax({
                url:"{{ route("admin.locations.update",[0]) }}",
                type: 'PUT',
                data: { id: id, number: number },
                success: function(response){
                    if (response.success) {
                        alert('Location Updated Successfully');
                        $('#get-location').trigger('click');
                        $("#editLocationModel").modal('hide');
                        $('#show_modal').trigger('click');
                       
                    }
                },

                error: function(response){
                    if (response.status == 422){
                        $('.update-danger').show(0).delay(3000).hide(400);
                        setTimeout(function() { 
                            $('.update-danger').fadeOut(); 
                        }, 3000);
                       $('.update-error').html(response.responseJSON.message.number);
                       // $('.update-error').html(response.responseJSON.message.name);
                    }
                }
            });
        });


        $('#storeLocation').click(function(e){
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // var name = $('#location_name').val();
            var number = $('#location_number').val();

            $.ajax({
                url: "{{ route('admin.locations.store') }}",
                method: 'post',
                data: { number: number },

                success: function(response){
                    console.log(response)
                    if (response.success) {
                        alert('Location created successfully');
                        $('#get-location').trigger('click');
                        // $('#location_name').val('');
                        $('#location_number').val('');
                        console.log(dataObject.data.push([
                                // name,
                                number, 
                                '<a data-id="'+response.location.original.location.location_id+'" class="editRecord editLocation btn btn-info btn-xs" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>'
                            ]));
                        $('#location_table').DataTable().clear().destroy();
                        $('#location_table').DataTable({
                            'columnDefs': [{
                                'targets': [1],
                                'orderable': false
                            }],
                            'scrollY':        '37vh',
                            'scrollCollapse': true,
                            'sPaginationType': 'full_numbers',
                            'data': dataObject.data,
                        });

                    }
                },

                error: function(response){
                    console.log(response)
                    if (response.status == 422){
                        $('.loc-danger').show(0).delay(3000).hide(400);
                        setTimeout(function() { 
                            $('.loc-danger').fadeOut(); 
                        }, 3000);
                        $('.loc-error').html(response.responseJSON.errors.number);
                        // $('.loc-error').html(response.responseJSON.errors.name);
                    }
                }
            });
        });

        $('#get-location').click(function(){
            $.ajax({
                type:"get",
                url:"{{ route('admin.locations.list') }}",
                data: {},
                dataType: 'json', 
                success:function(res){
                    var len = $.map(res, function(n, i) { return i; }).length;  
                    if(len){
                        $("#location").empty();
                        $("#location").append('<option value="">Select</option>');
                        $.each(sortReponse(res),function(key,value){
                            $("#location").append('<option value="'+value.key+'">'+value.value+'</option>');
                        });
                        $('#location').append('<option value="Add">Add</option>');
                    }
                }
            });  
        });

        function sortReponse(res)
        {
            var sorted = [];
            $(res).each(function(k, v) {
                for(var key in v) {
                    sorted.push({key: key, value: v[key]})
                }
            });
            return sorted.sort(function(a, b){
                if (a.value < b.value) return -1;
                if (a.value > b.value) return 1;
                return 0;
            });
        }

    });
</script>
@endsection
