@extends('layouts.layout')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Timber List</h4><br>
                <form> 
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-lg-5">
                        </div>
                        <div class="col-lg-3 ">
                            <input type="text" name="timber_number" id="timber_number" class="form-control float-right" placeholder="Timber number">
                        </div>
                        <div class="col-lg-3">
                            <input type="text" name="location_number" id="location_number" class="form-control" placeholder="Location number">
                        </div>
                            <a href="javascript:void(0)" class="btn btn-info btn-sm float-right" style="margin-top: 13px; margin-bottom: 13px;" id="search_timber">Search</a>
                    </div>
                </form>
                <table id="timber_table" class="table-responsive table table-bordered table-striped table-hover order-column">
                    <thead class="bg-color">
                        <tr>
                            {{-- <th>Timber Name</th> --}}
                            <th>Timber Number</th>
                            <th>Barcode</th>
                            {{-- <th>Location Name</th> --}}
                            <th>Location Number</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($timbers as $timber)
                        <tr>
                            {{-- <td>{{ $timber->name }}</td> --}}
                            <td>{{ $timber->number }}</td>
                            <td>{{ $timber->bar_code }}</td>
                            @if($timber->stock_deleted_at == '')
                                {{-- <td>{{ $timber->location_name }}</td> --}}
                                <td>{{ $timber->location_number }}</td>
                            @else
                                {{-- <td></td> --}}
                                <td></td>
                            @endif
                            <td>
                                <a class="editRecord btn btn-info btn-xs" href="{{ route('admin.timbers.edit', [$timber->timber_id]) }}"><i class="fa fa-pencil"></i> Edit</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
         
            </div>
        </div>
    </div>
</div>
<style>
.toolbar {
    float: left;
}
.timber-number{
    border: 1px solid red;
}
.location-number {
    border: 1px solid red;
}
</style>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){

        $('#timber_table').DataTable({
            'columnDefs': [{
                'targets': [3],
                'orderable': false
            }],
            'dom': 'l<"toolbar">frtip',
            'initComplete': function(){
                $("div.toolbar").html('<a class="btn btn-sm btn-info" href="{{ route('admin.timbers.create') }}"><i class="fa fa-plus"></i> Add Timber</a>');
            },
            'searching': false,
            'sPaginationType': 'full_numbers'
        });

        $('#search_timber').click(function(e){
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var timber_number   = $('#timber_number').val();
            var location_number = $('#location_number').val();
            var dataObject={"data":[]};

            $.ajax({
                url: "{{ route('admin.timber.search') }}",
                method: 'post',
                data: { timber_number: timber_number, location_number: location_number },

                success: function(response){
                    if (response.success) {
                        $('input#timber_number').removeClass('timber-number');
                        $('input#location_number').removeClass('location-number');
                        var objectLength=Object.keys(response.timbers).length;
                        for(var i=0; i<objectLength; i++){
                            dataObject.data.push(
                                [
                                    // response.timbers[i].name,
                                    response.timbers[i].number,
                                    response.timbers[i].bar_code,
                                    // response.timbers[i].location_name,
                                    response.timbers[i].location_number,
                                    '<a class="editRecord editimber btn btn-info btn-xs" href="/admin/timbers/'+response.timbers[i].timber_id+'/edit"><i class="fa fa-pencil"></i> Edit</a>'
                                ]);
                            }
                        console.log(dataObject);
                        $('#timber_table').DataTable().clear().destroy();
                        $('#timber_table').DataTable({
                            'columnDefs': [{
                                'targets': [3],
                                'orderable': false
                            }],
                            'dom': 'l<"toolbar">frtip',
                            'initComplete': function(){
                                $("div.toolbar").html('<a class="btn btn-sm btn-info" href="{{ route('admin.timbers.create') }}"><i class="fa fa-plus"></i> Add Timber</a>');
                            },
                            'searching': false,
                            'sPaginationType': 'full_numbers',
                            'data': dataObject.data
                        
                        });


                    }
                },

                error: function(response) {
                    if (response.status == 422) {
                        if (response.responseJSON.message.timber_number) {
                            $('input#timber_number').addClass('timber-number');
                        }
                        if (response.responseJSON.message.location_number) {
                            $('input#location_number').addClass('location-number');
                        }
                        
                    }
                }
            });
        });
        $('input#timber_number').keypress(function() {
            $(this).removeClass('timber-number');
        });
        $('input#location_number').keypress(function() {
            $(this).removeClass('location-number');
        });

    });
</script>
@endsection
