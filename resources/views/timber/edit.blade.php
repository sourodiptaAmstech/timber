@extends('layouts.layout')
@section('content')
{{-- <div class="row"> --}}
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class=""> <h4 class="card-title">Edit Timber</h4></div>

                    <a href="#" id="get-location" style="display: none;" class="btn btn-secondary">Click me</a>
                    <div class="row justify-content-center ">
                        <div class="col-lg-10">
                        <div class="row">
                            <div class="col-lg-6">
                            <div class="alert alert-danger edit-timber" role="alert" style="display:none">
                                <p class="mesg-error"></p>
                            </div>
                            </div>
                        </div>

                            <form>
                                <div class="row">
                                    {{-- <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="name" class="form-label text-dark">Timber Name</label>
                                            <input type="text" id="name" name="name" value="{{$timber->name}}" class="form-control" placeholder="Name" required>
                                        </div>
                                    </div> --}}
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="number" class="form-label text-dark">Timber Number</label>
                                            <input type="text" id="number" name="number" value="{{$timber->number}}" class="form-control" placeholder="Number" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    {{-- <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="bar_code" class="form-label text-dark">Barcode</label>
                                            <input type="text" id="bar_code" name="bar_code" value="{{$timber->bar_code}}" class="form-control" placeholder="Barcode" required>
                                        </div>
                                    </div> --}}
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="active" class="form-label text-dark">Status</label>
                                            <select name="active" id="active" class="form-control" required>
                                            <option value="">Select</option>
                                            <option value="1" {{ $timber->active == 1 ? 'selected' : '' }}>Active</option>
                                            <option value="0" {{ $timber->active == 0 ? 'selected' : '' }}>Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="location" class="form-label text-dark">Location Number</label>
                                            <span class="text-info pull-right" id="lode_location" style="display: none;"> Loading... <i class="fa  fa-cog fa-spin "></i></span>

                                            <select name="location" id="location" class="form-control">
                                                <option value="">Select</option>
                                                @foreach($locations as $location)
                                                <option value="{{$location->location_id}}" {{ $location->location_id == $timber->stocks['location_id'] ? 'selected' : '' }}>{{ $location->number }}</option>
                                                @endforeach
                                                <option value="Add">Add</option>
                                            </select>
                                        </div>
                                    </div>
                                   {{--  <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="grade" class="form-label text-dark">Grade</label>
                                            <input type="text" id="grade" name="grade" value="{{$timber->grade}}" class="form-control" placeholder="Grade" required>
                                        </div>
                                    </div> --}}
                                </div>

                                <div class="row col-lg-6">
                                    <a href="javascript:void(0)" class="btn btn-info float-left" data-id="{{$timber->timber_id}}" id="updateTimber">{{ trans('global.update') }}</a>
                                    <button class="btn btn-info float-left" style="display: none" id="loadTimber">Updating <i class='fa fa-circle-o-notch fa-spin'></i></button> &nbsp;
                                    <a href="{{ route('admin.timbers.index') }}" style="border: 1px solid #808890;" class="btn btn-outline-default">Cancel</a>
                                </div>
                            </form>
                            
                        </div>  
                    </div>

                </div>
            </div>
        </div>
    </div>
{{-- </div> --}}
    @include('timber.location')
@endsection
@section('scripts')
    @yield('script')
<script>
    $(document).ready(function () {

        $('#updateTimber').click(function(e){
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // var name = $('#name').val();
            var number = $('#number').val();
            // var bar_code = $('#bar_code').val();
            var active = $('#active').val();
            var location = $('#location').val();
            // var grade = $('#grade').val();
            var id = $(this).data('id');

            $('#updateTimber').css({display: 'none'});
            $('#loadTimber').css({display: 'block'});
            $.ajax({
                url:"{{ route("admin.timbers.update",[0]) }}",
                method: 'put',
                data: { id: id, number: number, bar_code: number, active: active, location: location },

                success: function(response){
                    $('#updateTimber').css({display: 'block'});
                    $('#loadTimber').css({display: 'none'});
                    if (response.success) {
                        alert('Timber updated successfully')
                        window.location.href = '{{ route('admin.timbers.index') }}'
                    }
                },

                error: function(response){
                    $('#updateTimber').css({display: 'block'});
                    $('#loadTimber').css({display: 'none'});
                    if (response.status == 422){
                        $('.edit-timber').show();
                        // $('.mesg-error').html(response.responseJSON.message.grade);
                        $('.mesg-error').html(response.responseJSON.message.location);
                        $('.mesg-error').html(response.responseJSON.message.active);
                        // $('.mesg-error').html(response.responseJSON.message.bar_code);
                        $('.mesg-error').html(response.responseJSON.message.number);
                        // $('.mesg-error').html(response.responseJSON.message.name);
                    }
                    if (response.status == 500){
                        $('.edit-timber').show();
                        $('.mesg-error').text('Internal server error');
                    }
                }
            });
        });

    });
</script>

@endsection
