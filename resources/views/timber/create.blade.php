@extends('layouts.layout')
@section('content')
{{-- <div class="row"> --}}

    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    
                    <div class=""> <h4 class="card-title">Add Timber</h4></div>
                    <a href="#" id="get-location" style="display: none;" class="btn btn-secondary">Click me</a>
                    <div class="row justify-content-center ">
                       
                        <div class="col-lg-5">
                            <div class="alert alert-danger add-timber" role="alert" style="display:none;">
                                <p class="mesg-error"></p>
                            </div>

                            <form>
                                {{-- <div class="row"> --}}
                                   {{--  <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="name" class="form-label text-dark">Timber Name</label>
                                            <input type="text" id="name" name="name" class="form-control" placeholder="Name" required>
                                        </div>
                                    </div> --}}
                                    {{-- <div class="col-lg-6"> --}}
                                        <div class="form-group">
                                            <label for="number" class="control-label">Timber Number</label>
                                            <input type="text" id="number" name="number" class="form-control" placeholder="Number" required>
                                        </div>
                                    {{-- </div>
                                </div> --}}

                               {{--  <div class="row"> --}}
                                    {{-- <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="bar_code" class="form-label text-dark">Barcode</label>
                                            <input type="text" id="bar_code" name="bar_code" class="form-control" placeholder="Barcode" required>
                                        </div>
                                    </div> --}}
                                    {{-- <div class="col-lg-6"> --}}
                                        <div class="form-group">
                                            <label for="location" class="form-label">Location Number</label>
                                            {{-- <input type="text" id="location" name="location" value="{{$location->number}}" class="form-control" readonly> --}}
                                            <select name="location" id="location" class="form-control" readonly>
                                                {{-- @foreach($locations as $location) --}}
                                                <option value="{{$location->location_id}}" >{{ $location->number }}</option>
                                                {{-- @endforeach --}}
                                            </select>
                                        </div>
                                    {{-- </div> --}}
                                {{-- </div> --}}
                               {{--  <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="grade" class="form-label text-dark">Grade</label>
                                            <input type="text" id="grade" name="grade" class="form-control" placeholder="Grade" required>
                                        </div>
                                    </div>
                                </div> --}}

                                {{-- <div class="row col-lg-6"> --}}
                                    <a href="javascript:void(0)" class="btn btn-info float-left" id="storeTimber">{{ trans('global.save') }}</a>&nbsp;
                                    <a href="{{ route('admin.timbers.index') }}" style="border: 1px solid #808890;" class="btn btn-outline-default">Cancel</a>
                                {{-- </div> --}}
                            </form>
                        </div>
                        <div class="col-lg-1">
                            <p class="text-dark">OR</p>
                        </div>
                        <div class="col-lg-6">
                            <div class="alert alert-danger add-csv" role="alert" style="display:none;">
                                <p class="csv-error" style="color: red;"></p>
                            </div>
                            <form id="importCsv" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                            <div class="form-group">
                                <label for="file" class="control-label">Import CSV file</label>
                                
                                <input type="file" name="file" id="file" class="form-control" required>
                                <input type="hidden" name="location" value="{{$location->location_id}}">
                            </div>
                        
                            <button type="submit" class="btn btn-info" name="submit"><div style="display: -webkit-inline-box;line-height: 18px;">Save&nbsp;<i class="fa fa-circle-o-notch fa-spin" id="reload" style="display: none;"></i></div></button>
                            <a href="{{ route('admin.timbers.index') }}" style="border: 1px solid #808890;" class="btn btn-outline-default">Cancel</a>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
   
{{-- </div> --}}
    {{-- @include('timber.location') --}}
@endsection
@section('scripts')
    {{-- @yield('script') --}}
<script>
    $(document).ready(function () {

        $('#storeTimber').click(function(e){
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // var name = $('#name').val();
            var number = $('#number').val();
            // var bar_code = $('#bar_code').val();
            var location = $('#location').val();
            // var grade = $('#grade').val();
           
            $.ajax({
                url: "{{ route('admin.timbers.store') }}",
                method: 'post',
                data: { number: number, location: location },

                success: function(response){
                    if (response.success) {
                        alert('Timber created successfully');
                        window.location.href = '{{ route('admin.timbers.index') }}';
                    }
                },

                error: function(response){
                    console.log(response)
                    if (response.status == 422){
                        $('.add-timber').show();
                        // $('.mesg-error').html(response.responseJSON.message.grade);
                        // $('.mesg-error').html(response.responseJSON.message.location);
                        // $('.mesg-error').html(response.responseJSON.message.bar_code);
                        $('.mesg-error').html(response.responseJSON.message.number);
                        // $('.mesg-error').html(response.responseJSON.message.name);
                    }
                    if (response.status == 500){
                        $('.add-timber').show();
                        $('.mesg-error').text('Internal server error');
                    }
                }
            });
        });

        $('#importCsv').on('submit', function(event){
            event.preventDefault();
            $('#reload').css({'display':'block'});
            $.ajax({
            url: "{{ route('admin.import-csv') }}",
            method:"POST",
            data:new FormData(this),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
            success: function(response){
                if (response.success) {
                    alert('CSV file imported successfully');
                    window.location.href = '{{ route('admin.timbers.index') }}';
                }
            },

            error: function(response){
                $('#reload').css({'display':'none'});

                if (response.status == 422){
                    $('.add-csv').show();
                    $('.csv-error').html(response.responseJSON.errors.file);
                }
                if (response.status == 401){
                    $('.add-csv').show();
                    $('.csv-error').html(response.responseJSON.message);
                }
                if (response.status == 500){
                    $('.add-csv').show();
                    $('.csv-error').text('Internal server error');
                }
            }
          });
        });

    });
</script>

@endsection
