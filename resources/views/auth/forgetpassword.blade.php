@extends('layouts.app')
@section('content')
<div class="row justify-content-center">
    <div class="col-md-6 login-margin">
        {{-- <div class="card-group"> --}}
            <div class="card p-4">
                <div class="card-body">
                    <div class="login-logo">
                        <img src="{{ asset('/images/Logo.png') }}" class="">
                    </div>

                    <h5><p class="text-info">Reset Password</p></h5>
                    <div class="alert alert-success fade show" role="alert" style="display:none">
                        <p class="mesg-success"></p>
                    </div>
                    <div class="alert alert-danger fade show" role="alert" style="display:none">
                        <p class="mesg-error"></p>
                    </div>
                
                    <form>
                       
                        {{ csrf_field() }}
                        
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-user"></i></span>
                            </div>
                            <input type="text" name="email" id="email" class="form-control" placeholder="{{ trans('global.login_email') }}">
                            <span class="text-danger">{{$errors->first('email')}}</span>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <input type="submit" id="ajaxSubmit" class="btn btn-info px-4" value='Send'>
                                <button type="button" style="display: none;" id="btn_load" class="btn btn-info px-2">Sending <i class='fa fa-circle-o-notch fa-spin'></i></button>
                            </div>
                            <div class="col-6 text-right">
                                <a class="btn btn-link px-0" href="{{ route('/') }}">
                                    Login Here!
                                </a>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        {{-- </div> --}}
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){

        $('#ajaxSubmit').click(function(e){
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var email = $('#email').val();
            $('#ajaxSubmit').css({'display':'none'});
            $('#btn_load').css({'display':'block'});
            $.ajax({
                url: "{{ url('forget-password') }}",
                method: 'post',
                data: { email: email },
                success: function(response){
                    $('#btn_load').css({'display':'none'});
                    $('#ajaxSubmit').css({'display':'block'});
                    if (response.success) {
                        $('.alert-success').show(0).delay(4000).hide(400);
                        setTimeout(function() { 
                            $('.alert-success').fadeOut(); 
                        }, 4000);
                        $('.mesg-success').html(response.message);
                    }
                },
                error: function(response){
                    $('#btn_load').css({'display':'none'});
                    $('#ajaxSubmit').css({'display':'block'});
                    if (response.status == 422){
                        $('.alert-danger').show(0).delay(4000).hide(400);
                        setTimeout(function() { 
                            $('.alert-danger').fadeOut(); 
                        }, 4000);  
                        $('.mesg-error').html(response.responseJSON.errors.email[0]);
                        $('.mesg-error').html(response.responseJSON.errors.email[1]);
                    }
                    if (response.status == 404){
                        $('.alert-danger').show(0).delay(4000).hide(400);
                        setTimeout(function() { 
                            $('.alert-danger').fadeOut(); 
                        }, 4000);
                        $('.mesg-error').html(response.responseJSON.message);
                    }
                    if (response.status == 500){
                        $('.alert-danger').show(0).delay(4000).hide(400);
                        setTimeout(function() { 
                            $('.alert-danger').fadeOut(); 
                        }, 4000);
                        $('.mesg-error').text('Internal server error');
                    }
                }
            });
        });
    });

</script>
@endsection