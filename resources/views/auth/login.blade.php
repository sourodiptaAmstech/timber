@extends('layouts.app')
@section('content')
<div class="row justify-content-center">
    <div class="col-md-6 login-margin">
        <div class="card px-4">
            <div class="card-body">
                <div class="login-logo">
                     <img src="{{ asset('/images/Logo.png') }}" class="">
                </div>
                <h5>
                   <p class="text-info">Admin Login</p>
                </h5>

                @if(count($errors) > 0 )
                <div class="alert alert-danger fade show" style="color: red;" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <ul class="p-0 pb-2 m-0" style="list-style: none;">
                        @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                
                @if(\Session::has('message'))
                    <p class="alert alert-danger fade show" style="height: 46px; color: red;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                        {{ \Session::get('message') }}
                    </p>
                @endif

                <form method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-user"></i></span>
                        </div>
                        <input name="email" type="text" class="form-control" placeholder="{{ trans('global.login_email') }}">
                    </div>
                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-lock"></i></span>
                        </div>
                        <input name="password" type="password" class="form-control" placeholder="{{ trans('global.login_password') }}">
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <input type="submit" class="btn btn-info px-4" value='{{ trans('global.login') }}'>
                        </div>
                        <div class="col-6 text-right">
                            <a class="btn btn-link px-0" href="{{ route('forget-password') }}">
                                {{ trans('global.forgot_password') }}
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection